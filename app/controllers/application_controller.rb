class ApplicationController < ActionController::API
    def render_resource(resource)
        if resource.errors.empty?
          render json: resource
        else
          validation_error(resource)
        end
    end
    
    def validation_error(resource)
        render json: {
          errors: [
            {
              status: '400',
              title: 'Bad Request',
              detail: resource.errors,
              code: '100'
            }
          ]
        }, status: :bad_request
    end

    rescue_from CanCan::AccessDenied do |exception|
      render json: {message: "you are not allowed!"}, status: :forbidden

    end
end
